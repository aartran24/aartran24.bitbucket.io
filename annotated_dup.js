var annotated_dup =
[
    [ "cotask", null, [
      [ "Task", "classcotask_1_1Task.html", "classcotask_1_1Task" ],
      [ "TaskList", "classcotask_1_1TaskList.html", "classcotask_1_1TaskList" ]
    ] ],
    [ "EncoderDriver", null, [
      [ "EncoderDriver", "classEncoderDriver_1_1EncoderDriver.html", "classEncoderDriver_1_1EncoderDriver" ]
    ] ],
    [ "MotorDriver", null, [
      [ "DRV8847", "classMotorDriver_1_1DRV8847.html", "classMotorDriver_1_1DRV8847" ],
      [ "DRV8847_channel", "classMotorDriver_1_1DRV8847__channel.html", "classMotorDriver_1_1DRV8847__channel" ]
    ] ],
    [ "TouchDriver", null, [
      [ "TouchDriver", "classTouchDriver_1_1TouchDriver.html", "classTouchDriver_1_1TouchDriver" ]
    ] ]
];